
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;

public class FuelSensor extends Agent{
	public String tractorNr;
	public String Port;
	public String Msg = "request";
	public String FuelConsumption = "";
	protected void setup() {
		System.out.println("Started "+getAID().getLocalName());
		Object[] args = getArguments();//args: (str tractorID)
		tractorNr = (String)args[0];
		Port = Integer.toString(9000+Integer.parseInt(tractorNr));
		
		//Register service: standard for all agents,just change info in sd.setType
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("tractor"+tractorNr+"Fuel");
		sd.setName("Tractor-DigitalTwins");
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		addBehaviour(new TickerBehaviour(this, 5000) {
			protected void onTick() {
				
				// Perform the request
				myAgent.addBehaviour(new GetSensorInfo());
			}
		} );
		MessageTemplate template = MessageTemplate.and(
		  		MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
		  		MessageTemplate.MatchPerformative(ACLMessage.REQUEST) );
		addBehaviour(new SendFuelConsumption(this,template));
	}
	
	private class GetSensorInfo extends  Behaviour{
		private String dataReceived;
		private DataOutputStream outToServer;
		private boolean done = false;
		public void action() {
			
		try {				
					//setup socket
					Socket socket = new Socket("localhost", Integer.parseInt(Port));
					
					//send message over socket
					outToServer = new DataOutputStream(socket.getOutputStream());
					byte[] outByteString = Msg.getBytes("UTF-8");
					outToServer.write(outByteString);
					
					//read replied message from socket
					byte[] inByteString = new byte[500] ;
		            int numOfBytes = socket.getInputStream().read(inByteString);
		            FuelConsumption = new String(inByteString, 0, numOfBytes, "UTF-8");
					//close connection
					socket.close();
					done = true;
				
				
				//wait(1000);
		}
		catch (IOException e) {
			e.printStackTrace();
		} 
	}
		public boolean done() {
			return (done);
		}
	}
	public class SendFuelConsumption extends AchieveREResponder{

		public SendFuelConsumption(Agent a, MessageTemplate mt) {
			super(a, mt);
			// TODO Auto-generated constructor stub
		}
		protected ACLMessage prepareResponse(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			if (request.getContent().equals("fuelFortractor"+tractorNr)) {
				ACLMessage agree = request.createReply();
				agree.setPerformative(ACLMessage.AGREE);
				return agree;
			}
			else {
				throw new RefuseException("check-failed");
			}
		}
		
		protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response) throws FailureException {
			if ((request.getContent().equals("fuelFortractor"+tractorNr)) &&(FuelConsumption!="")){
				ACLMessage inform = request.createReply();
				inform.setPerformative(ACLMessage.INFORM);
				inform.setContent(FuelConsumption);
				return inform;
			}
			else {
				System.out.println("Agent "+getLocalName()+": Action failed");
				throw new FailureException("unexpected-error");
			}	
		}
		
	}
	
}
