
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

import java.awt.*;
import java.awt.event.*;
import java.util.Date;
import java.util.Vector;

import javax.swing.*;
import javax.swing.border.Border;

public class TractorsGUI extends JFrame{
private UserInterface GUIAgent;
public ACLMessage expandReq = new ACLMessage(ACLMessage.REQUEST);
	
	private JTextField titleField, priceField;
	private int nrFarms = 4;
	private int nrColsPerFarm = 3;
	private int nrRowsPerFarm = 2;
	private JPanel outputPanel = new JPanel();
	private JPanel inputPanel = new JPanel();
	private JPanel[] farmPanel = {new JPanel(),new JPanel(),new JPanel(),new JPanel()};
	private JLabel farmName[] = new JLabel[nrFarms];
	private JLabel colName[] = new JLabel[nrFarms*nrColsPerFarm];
	private JLabel rowName[] = new JLabel[nrFarms*nrRowsPerFarm];
	private JPanel tractorPanel[] = new JPanel[nrFarms*nrColsPerFarm*nrRowsPerFarm];
	private JLabel tractor[] = new JLabel[tractorPanel.length];
	private JLabel fuel[] = new JLabel[tractorPanel.length];
	JTextArea messageBox = new JTextArea();	
	TractorsGUI(UserInterface a) {
		super(a.getLocalName());
		
		GUIAgent = a;
		this.setLayout(new GridBagLayout());
		outputPanel.setLayout(new GridLayout(2,2));
		
		for(int c=0;c<colName.length;c++) {
			colName[c] = new JLabel("Column "+String.valueOf(c%nrColsPerFarm+1));
		}	
		
		for(int r=0;r<rowName.length;r++) {
			rowName[r] = new JLabel("Row "+String.valueOf(r%nrRowsPerFarm+1));
		}
	
		for(int i = 0;i<tractorPanel.length;i++){
			tractor[i] = new JLabel("");
			fuel[i] = new JLabel("");
			tractorPanel[i] = new JPanel();
			tractorPanel[i].setLayout(new GridLayout(2,1));
			tractorPanel[i].setBorder(BorderFactory.createLineBorder(Color.black));
			tractorPanel[i].setBackground(Color.gray);
			tractorPanel[i].add(tractor[i]);
			tractorPanel[i].add(fuel[i]);			
		}

		for (int i = 0;i<4;i++) {
			farmPanel[i] = new JPanel();
			farmPanel[i].setLayout(new GridLayout(3,4));
			farmPanel[i].setBorder(BorderFactory.createLineBorder(Color.orange));
			farmName[i] = new JLabel("Farm "+String.valueOf(i+1));
			farmName[i].setFont(farmName[i].getFont().deriveFont(Font.BOLD, 20f));
			farmPanel[i].add(farmName[i]);
			for (int c = 0;c<nrColsPerFarm;c++) {
				farmPanel[i].add(colName[i*nrColsPerFarm+c]);
			}
			for (int r=0;r<nrRowsPerFarm;r++) {
				farmPanel[i].add(rowName[i*nrRowsPerFarm+r]);
				for(int t=0;t<nrColsPerFarm;t++) {
					farmPanel[i].add(tractorPanel[i*nrColsPerFarm*nrRowsPerFarm+r*nrColsPerFarm+t]);
				}
			}
			outputPanel.add(farmPanel[i]);	
		
		}		
		inputPanel.setLayout(new GridLayout(1,2));
		JButton newTractor = new JButton("New Tractor");
		newTractor.addActionListener(new ActionListener(){  
		   public void actionPerformed(ActionEvent e){  
		        GetExpander("newTractor");
		        GUIAgent.addBehaviour(new RequestNewTractorOrFarm(GUIAgent,expandReq));
		        //new XmlToObject();
		   }  
	     });  
		inputPanel.add(newTractor);
		JButton newFarm = new JButton("New Farm");
		newFarm.addActionListener(new ActionListener(){  
		   public void actionPerformed(ActionEvent e){  
			   GetExpander("newFarm");
		        GUIAgent.addBehaviour(new RequestNewTractorOrFarm(GUIAgent,expandReq));
		   }  
	     });  
		inputPanel.add(newFarm);
		
		messageBox.insert("Error", 0);
		/////////////////////////////
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;  
	    gbc.gridx = 0;  
	    gbc.gridy = 0; 
	    gbc.ipady = 300;
		this.add(outputPanel,gbc);
	    gbc.gridx = 0;  
	    gbc.gridy = 1;  
	    gbc.ipady = 5;
		this.add(inputPanel,gbc);
	    gbc.gridx = 0;  
	    gbc.gridy = 2;  
	    gbc.ipady = 60;
		this.add(messageBox,gbc);
	
		// Make the agent terminate when the user closes 
		// the GUI using the button on the upper right corner	
		addWindowListener(new	WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				GUIAgent.doDelete();
			}
		} );
		
		setResizable(true);
	}
	
	public void showGui() {
		pack();
		super.setSize(1200,800);
		super.setVisible(true);
	}	
	public void updateGui(int farmID,int row, int col, String TractorID, String FuelConsumption) {
		int changedLoc = farmID*nrColsPerFarm*nrRowsPerFarm+nrColsPerFarm*row+col;
		tractor[changedLoc].setText(TractorID);
		fuel[changedLoc].setText("Fuel consumption: "+FuelConsumption);
		tractorPanel[changedLoc].setBackground(Color.green);
		for(int i = 0;i<nrFarms*nrColsPerFarm*nrRowsPerFarm;i++) {
			if((tractor[i].getText().equals(TractorID))&&(i!=changedLoc)) {
				tractor[i].setText("");
				fuel[i].setText("");
				tractorPanel[i].setBackground(Color.gray);
			}
		}
	}
	public void GetExpander(String messageContent) {
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Expand");
		template.addServices(sd);
		try {
			DFAgentDescription[] result = DFService.search(GUIAgent, template); 
			expandReq.clearAllReceiver();
			if(result.length==1) {
				System.out.println("Found the following expansion agents for "+GUIAgent.getLocalName()+":");
				expandReq.addReceiver(result[0].getName());
				expandReq.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
				// We want to receive a reply in 2 secs
				expandReq.setReplyByDate(new Date(System.currentTimeMillis() + 2000));
				expandReq.setContent(messageContent);
			}
			
		}catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}
	
	public class RequestNewTractorOrFarm extends AchieveREInitiator{

		public RequestNewTractorOrFarm(Agent a, ACLMessage msg) {
			super(a, msg);
			// TODO Auto-generated constructor stub
		}
		protected void handleInform(ACLMessage inform) {
			System.out.println("Agent "+inform.getSender().getName()+" successfully added "+inform.getContent());
			messageBox.insert(inform.getContent()+"\n", 0);
		}
		protected void handleRefuse(ACLMessage refuse) {
			System.out.println("Agent "+refuse.getSender().getName()+" could not add "+refuse.getContent());
			messageBox.insert(refuse.getContent()+"\n", 0);
		}
		protected void handleFailure(ACLMessage failure) {
			if (failure.getSender().equals(myAgent.getAMS())) {
				// FAILURE notification from the JADE runtime: the receiver
				// does not exist
				System.out.println("Expander does not exist");
				messageBox.insert("Error: Expanding agent not existing. Contact support line\n", 0);
			}
			else {
				messageBox.insert("Error: Expanding agent malfunctioned. Contact support line\n", 0);
				System.out.println("Agent "+failure.getSender().getName()+" failed to add the requested tractor/farm");
			}
		}
		protected void handleAllResultNotifications(Vector notifications) {
				//Responder didn't reply within the specified timeout
			if(notifications.size()<1)
				System.out.println("Timeout expired for expander to add a new tractor/farm");	
				messageBox.insert("Error: Expanding agent took too long\n", 0);
		}
	}
	
}
