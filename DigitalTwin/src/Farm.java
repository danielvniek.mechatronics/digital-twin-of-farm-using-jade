import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.ContractNetInitiator;
import jade.proto.ContractNetResponder;


//The job of this agent is to get location info from farm and send this to tractor agents
public class Farm extends Agent{
	String Port;	
	String farmNr;
	private int locRows = 2;//just change locRows,locCols and nrSensors if more sensors added; not giving this as argument to prevent error
	private int locCols = 3;
	public int nrSensors = 6;
	String[] MsgPort = new String[nrSensors];
	public LocationData locations[] = new LocationData[nrSensors];

	private int arrayCount = 0;
	
	protected void setup() {
		System.out.println("Started "+getAID().getLocalName());
		Object[] args = getArguments();//args: (str farm,str loc) ex: (1,p11)
		farmNr = (String)args[0];
		Port = Integer.toString(9100+Integer.parseInt(farmNr));
		for(int i = 0;i<locRows;i++) {
			for(int j = 0; j<locCols;j++) {
				locations[arrayCount] = new LocationData();
				String pos = Integer.toString(i+1)+Integer.toString(j+1);
				MsgPort[arrayCount] = "farm"+farmNr + "_p"+pos;
				locations[arrayCount].position = "p"+pos;
			//	System.out.println(locations[arrayCount].position);
				arrayCount = arrayCount + 1;
			}
		}
		
		//Register service: standard for all agents,just change info in sd.setType
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("TractorLocation");
		sd.setName("Tractor-DigitalTwins");
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		addBehaviour(new TickerBehaviour(this, 5000) {
			protected void onTick() {
				
				// Perform the request
				myAgent.addBehaviour(new GetSensorInfo());
			}
		} );
		MessageTemplate template = MessageTemplate.and(
				MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET),
				MessageTemplate.MatchPerformative(ACLMessage.CFP) );
		addBehaviour(new SendLocations(this,template));
		
	
	}
	// Put agent clean-up operations here
	protected void takeDown() {
		// Deregister from the yellow pages
		try {
			DFService.deregister(this);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
		// Printout a dismissal message
		System.out.println("Farm "+getAID().getName()+" terminating.");
	}

	//BEHAVIOURS

	private class GetSensorInfo extends  Behaviour{
		private String dataReceived;
		private DataOutputStream outToServer;
		private int sensorNr=0;
		
		public void action() {
		try {
				for(int i = 0;i<nrSensors;i++) {
					//setup socket
					Socket socket = new Socket("localhost", Integer.parseInt(Port));
					//send message over socket
					outToServer = new DataOutputStream(socket.getOutputStream());
					byte[] outByteString = MsgPort[sensorNr].getBytes("UTF-8");
					outToServer.write(outByteString);
					
					//read replied message from socket
					byte[] inByteString = new byte[500] ;
		            int numOfBytes = socket.getInputStream().read(inByteString);
		            dataReceived = new String(inByteString, 0, numOfBytes, "UTF-8");
		            String[] dataReceivedSplit = dataReceived.split("_");
		            locations[sensorNr].tractorName = dataReceivedSplit[2];
		            try {
		            	locations[sensorNr].timeStamp = Integer.valueOf(dataReceivedSplit[3]);
		            	}
		            	catch(Exception e) {
		       
		            	}
					//close connection
					socket.close();
					sensorNr = sensorNr + 1;
				}
				
				//wait(1000);
		}
		catch (IOException e) {
			e.printStackTrace();
		} 
	}
		public boolean done() {
			return (sensorNr == 6);
		}
	}
	
	public class SendLocations extends ContractNetResponder {


		public SendLocations(Agent a, MessageTemplate mt) {
			super(a, mt);
			// TODO Auto-generated constructor stub
		}

		protected ACLMessage handleCfp(ACLMessage cfp) throws NotUnderstoodException, RefuseException {
			int proposal = 0;
			if (cfp.getContent()!=null) {
				String tractorID = cfp.getContent();
				int timeNewest = -1;
				ACLMessage propose = cfp.createReply();
				for(int i = 0;i<nrSensors;i++) {
					if((tractorID.equals(locations[i].tractorName))&&(locations[i].timeStamp>timeNewest)) {					
						timeNewest = locations[i].timeStamp;
					}
				}
				
				if(timeNewest>0) {
					// We provide a proposal	
					propose.setPerformative(ACLMessage.PROPOSE);
					propose.setContent(String.valueOf(timeNewest));
					return propose;
				}
				else {
					throw new RefuseException("evaluation-failed");
				}
				
			}
			else {
				throw new RefuseException("evaluation-failed");
			}
		}

		//@Override
		protected ACLMessage handleAcceptProposal(ACLMessage cfp, ACLMessage propose,ACLMessage accept) throws FailureException {
			if (cfp.getContent()!=null) {
				String tractorID = cfp.getContent();
				int timeNewest = -1;
				ACLMessage inform = accept.createReply();
				for(int i = 0; i<nrSensors;i++) {	
					if((tractorID.equals(locations[i].tractorName))&&(locations[i].timeStamp>timeNewest)) {
						inform.setContent(locations[i].position);
						timeNewest = locations[i].timeStamp;
					}
				}
				inform.setPerformative(ACLMessage.INFORM);
				return inform;
			}
			else {
				throw new FailureException("unexpected-error");
			}	
		}

		protected void handleRejectProposal(ACLMessage cfp, ACLMessage propose, ACLMessage reject) {
			}
	}
	

	


}
