import java.time.LocalDate;
import java.time.LocalTime;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement 
public class TractorDataXML {
		@XmlElement
	   public String TractorID;
	   @XmlElement 
	   public String FuelConsumption;
	   @XmlElement
	   public String Farm;
	   @XmlElement 
	   public int Column;	   
	   @XmlElement
	   public int Row;
	   @XmlElement
	   public int locationTimeStamp;
	   @XmlElement
	   public String dateAndTimeRecorded;
	   

}
