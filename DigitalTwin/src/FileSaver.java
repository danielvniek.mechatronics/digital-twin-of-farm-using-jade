import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class FileSaver extends Agent{
	
	String excelFilePath = "TractorData.xlsx";
	protected void setup() {
		System.out.println("FileSaver started");
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("SaveTractorData");
		sd.setName("Tractor-DigitalTwins");
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
		addBehaviour(new TractorInfoServer());
		addBehaviour(new NewTractorServer());
		
	}
	private class TractorInfoServer extends CyclicBehaviour {
		public void action() {
			MessageTemplate tractorInfo = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
			ACLMessage msg = myAgent.receive(tractorInfo);
			if (msg != null) {
				// Read tractor info
				String message = msg.getContent();
				  JAXBContext jaxbContext = null;
				try {
					jaxbContext = JAXBContext.newInstance(Tractor.class);
				} catch (JAXBException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	            Unmarshaller jaxbUnmarshaller = null;
				try {
					jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				} catch (JAXBException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
	            StringReader sr = new StringReader(message);
	            TractorDataXML T = null;
				try {
					T = (TractorDataXML)jaxbUnmarshaller.unmarshal(sr);
				} catch (JAXBException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	           try {
				updateFile(T);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	          
			}
			else {
				block();
			}
		}
	}
	public void updateFile(TractorDataXML T) throws IOException {
            FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
            Workbook workbook = WorkbookFactory.create(inputStream);
            Sheet sheet = workbook.getSheet(T.TractorID);
            if(sheet!=null) {
                 int rowNr = sheet.getLastRowNum()+1;
                 int columnCount = 0;   
                 Row row = sheet.createRow(rowNr);
                 Cell cell = row.createCell(columnCount);
                 cell.setCellValue(T.dateAndTimeRecorded);
                 columnCount++;
                 cell = row.createCell(columnCount);
                 cell.setCellValue(T.FuelConsumption);
                 columnCount++;
                 cell = row.createCell(columnCount);
                 cell.setCellValue(T.Farm);
                 columnCount++;
                 cell = row.createCell(columnCount);
                 cell.setCellValue(T.Column);
                 columnCount++;
                 cell = row.createCell(columnCount);
                 cell.setCellValue(T.Row);
                 columnCount++;
                 cell = row.createCell(columnCount);
                 cell.setCellValue(T.locationTimeStamp);
                 columnCount++;
      
            }else {
            	System.out.println("Error:Tractor sheet unavailable for "+T.TractorID);
            }
           
            inputStream.close();
 
            FileOutputStream outputStream = new FileOutputStream(excelFilePath);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();   
    }
	
	
	
	private class NewTractorServer extends CyclicBehaviour {
		
		private boolean success=true;
		private String tractorID;
		public void action() {
			//System.out.println("NewTractorServer running");
			MessageTemplate newTractor = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
			ACLMessage msg = myAgent.receive(newTractor);
			if (msg != null) {
				System.out.println("Filesaver requested to add new tractor");
				// Read tractor info
				String message = msg.getContent();
				     if(msg.getContent().contains("new:")) {
	            	tractorID = msg.getContent().substring(4);
	            	try {
						NewSheet(tractorID);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						success=false;
						e.printStackTrace();
					}
	            	ACLMessage reply = msg.createReply();
	                if(success==false) {
	                	reply.setPerformative(ACLMessage.FAILURE);
	                	reply.setContent(tractorID);
	                }else {
	                	reply.setPerformative(ACLMessage.AGREE);
	                }
	            }       
			}
			else {
				block();
			}
		}
	}
	public void NewSheet(String tractorID) throws IOException {
		boolean AddNewSheet = true;
		 FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
         Workbook workbook = WorkbookFactory.create(inputStream);
         int nrTractorSheets = workbook.getNumberOfSheets();//one NOTHING sheet   
         String Sheets[] = new String[nrTractorSheets];
         for(int i = 0;i<nrTractorSheets;i++) {
        	 if(tractorID.equals(workbook.getSheetName(i))) {
        		 AddNewSheet = false;
        	 }
         }
         inputStream.close();
         FileOutputStream outputStream = new FileOutputStream(excelFilePath);
         if(AddNewSheet==false) {
        	 System.out.println("Sheet already exists");
         }
         else {
        	 workbook.createSheet(tractorID);
        	 workbook.write(outputStream);
         }    
         

         workbook.close();
         outputStream.close();   
   	 
	}
	
	 
}
