
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.*;

import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREInitiator;
import jade.proto.ContractNetInitiator;

public class Tractor extends Agent{
	//PROPs
	public TractorDataXML T = new TractorDataXML();	
	public int nResponders;
	public String tractorID = "";
	ACLMessage msgLoc = new ACLMessage(ACLMessage.CFP);
	ACLMessage msgFuel = new ACLMessage(ACLMessage.REQUEST);
	public class getAvailableFuelSensors extends OneShotBehaviour{
		public void action() {
			DFAgentDescription template = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType(tractorID+"Fuel");
			template.addServices(sd);
			try {
				DFAgentDescription[] result = DFService.search(myAgent, template); 
				msgFuel.clearAllReceiver();
				if(result.length==1) {
					msgFuel.addReceiver(result[0].getName());
				}
				
			}catch (FIPAException fe) {
				fe.printStackTrace();
			}
	}}
	public class getAvailableFarms extends OneShotBehaviour {
		public void action() {
			DFAgentDescription template = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("TractorLocation");
			template.addServices(sd);
			try {
				DFAgentDescription[] result = DFService.search(myAgent, template); 
				
				msgLoc.clearAllReceiver();
				for (int i = 0; i < result.length; ++i) {
					msgLoc.addReceiver(result[i].getName());
				}
				nResponders = result.length;
			}		
			catch (FIPAException fe) {
				fe.printStackTrace();
			}
		}
	}
	protected void setup() {
		
		Object[] args = getArguments();//args: (str tractorID)	
		T.TractorID = "Tractor"+args[0];
		tractorID = "tractor"+args[0];
		System.out.println("Started "+ tractorID);
		//Register service: standard for all agents,just change info in sd.setType
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("TractorData");
		sd.setName("Tractor-DigitalTwins");
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
		// Fill the CFP message

			msgLoc.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
			// We want to receive a reply in 1 secs
			msgLoc.setReplyByDate(new Date(System.currentTimeMillis() + 1000));
			msgLoc.setContent(tractorID);
			msgFuel.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
			// We want to receive a reply in 1 secs
			msgFuel.setReplyByDate(new Date(System.currentTimeMillis() + 1000));
			msgFuel.setContent("fuelFor"+tractorID);
		//order or events
		
		addBehaviour(new TickerBehaviour(this, 6000) {
			protected void onTick() {			
				// Perform the request
				
				SequentialBehaviour fuelLocSaveDisp = new SequentialBehaviour(myAgent);
				fuelLocSaveDisp.addSubBehaviour(new getAvailableFuelSensors());
				fuelLocSaveDisp.addSubBehaviour(new getAvailableFarms());	
				fuelLocSaveDisp.addSubBehaviour(new RequestFuel(myAgent,msgFuel));
				fuelLocSaveDisp.addSubBehaviour(new RequestLocations(myAgent,msgLoc));	
				fuelLocSaveDisp.addSubBehaviour(new DisplayData());
				/*fuelLocSaveDisp.addSubBehaviour(new OneShotBehaviour() {
					public void action() {
						//System.out.println("SEQ DONE");
					}
				});*/
				myAgent.addBehaviour(fuelLocSaveDisp);			
			}
		} );
	
	}
	// Put agent clean-up operations here
	protected void takeDown() {
		// Deregister from the yellow pages
		try {
			DFService.deregister(this);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
		// Printout a dismissal message
		System.out.println("Farm "+getAID().getName()+" terminating.");
	}

	//BEHAVIOURS
	public class RequestFuel extends AchieveREInitiator{

		public RequestFuel(Agent a, ACLMessage msg) {
			super(a, msg);
			// TODO Auto-generated constructor stub
		}
		protected void handleInform(ACLMessage inform) {
			T.FuelConsumption = inform.getContent();
		}
		protected void handleRefuse(ACLMessage refuse) {
				}
		protected void handleFailure(ACLMessage failure) {
			if (failure.getSender().equals(myAgent.getAMS())) {
				// FAILURE notification from the JADE runtime: the receiver
				// does not exist
				System.out.println("Responder does not exist");
			}
			else {
				System.out.println("Agent "+failure.getSender().getName()+" failed to perform the requested action");
			}
		}
		protected void handleAllResultNotifications(Vector notifications) {
				//Responder didn't reply within the specified timeout
			if(notifications.size()<1)
				System.out.println("Timeout expired for fuelSensor");		
		}
	}
	public class RequestLocations extends ContractNetInitiator {
		
		public RequestLocations(Agent a, ACLMessage cfp) {
			super(a, cfp);
			// TODO Auto-generated constructor stub
		}

		protected void handlePropose(ACLMessage propose, Vector v) {
			
		}
		
		protected void handleRefuse(ACLMessage refuse) {
			
		}
		
		protected void handleFailure(ACLMessage failure) {
			if (failure.getSender().equals(myAgent.getAMS())) {
				// FAILURE notification from the JADE runtime: the receiver
				// does not exist
				System.out.println("Responder does not exist");
			}
			else {
				System.out.println("Agent "+failure.getSender().getName()+" failed");
			}
			// Immediate failure --> we will not receive a response from this agent
			nResponders--;
		}
		
		protected void handleAllResponses(Vector responses, Vector acceptances) {
			if (responses.size() < nResponders) {
				// Some responder didn't reply within the specified timeout
				System.out.println("Timeout expired: missing "+(nResponders - responses.size())+" responses");
			}
			// Evaluate proposals.
			int bestProposal = -1;
			AID bestProposer = null;
			ACLMessage accept = null;
			Enumeration e = responses.elements();
			while (e.hasMoreElements()) {
				ACLMessage msg = (ACLMessage) e.nextElement();
				if (msg.getPerformative() == ACLMessage.PROPOSE) {
					ACLMessage reply = msg.createReply();
					reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
					acceptances.addElement(reply);
					int proposal = Integer.parseInt(msg.getContent());
					if (proposal > bestProposal) {
						bestProposal = proposal;
						bestProposer = msg.getSender();
						accept = reply;
					}
				}
			}
			// Accept the proposal of the best proposer
			if (accept != null) {
				accept.setContent(tractorID);
				accept.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
				T.Farm = bestProposer.getLocalName();
				T.locationTimeStamp = bestProposal;
			}						
		}
		
		protected void handleInform(ACLMessage inform) {
			T.Row = Integer.parseInt(inform.getContent().substring(1, 2));
			T.Column = Integer.parseInt(inform.getContent().substring(2));
		}
		}
	
	public class DisplayData extends OneShotBehaviour{
		private String xmlContent;
	
		public void action() {
			Date date = Calendar.getInstance().getTime();  
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");  
			T.dateAndTimeRecorded = dateFormat.format(date); 
			  try
		        {
		            //Create JAXB Context
		            JAXBContext jaxbContext = JAXBContext.newInstance(TractorDataXML.class);
		             
		            //Create Marshaller
		            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		 
		            //Required formatting??
		            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		 
		            //Print XML String to Console
		            StringWriter sw = new StringWriter();
		             
		            //Write XML to StringWriter
		            jaxbMarshaller.marshal(T, sw);
		             
		            //Verify XML Content
		            xmlContent = sw.toString();
		        } catch (JAXBException e) {
		            e.printStackTrace();
		        }
				DFAgentDescription template = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("DisplayTractorData");
				template.addServices(sd);
				try {
					DFAgentDescription[] result = DFService.search(myAgent, template); 
					if(result.length==1) {
						ACLMessage inform = new ACLMessage(ACLMessage.PROPAGATE);
						inform.addReceiver(result[0].getName());
						inform.setContent(xmlContent);
						myAgent.send(inform);
					}
					
				}catch (FIPAException fe) {
					fe.printStackTrace();
				}
				template = new DFAgentDescription();
				sd = new ServiceDescription();
				sd.setType("SaveTractorData");
				template.addServices(sd);
				try {
					DFAgentDescription[] result = DFService.search(myAgent, template); 
					if(result.length==1) {
						ACLMessage inform = new ACLMessage(ACLMessage.INFORM);
						inform.addReceiver(result[0].getName());
						inform.setContent(xmlContent);
						myAgent.send(inform);
					}
					
				}catch (FIPAException fe) {
					fe.printStackTrace();
				}
		}
	}
}
