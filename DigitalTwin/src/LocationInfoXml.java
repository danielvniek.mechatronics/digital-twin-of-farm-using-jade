import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement 
public class LocationInfoXml {
		@XmlElement
	   String Farm;
	   @XmlElement 
	   String Location;
	   @XmlElement
	   String Tractor;
	   @XmlElement 
	   int Time;	   

}

