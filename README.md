# Description #
A farmer owns multiple farms and multiple tractors that work the lands on these farms. A farmer has installed a fuel consumption sensors, an IoT device and an RFID tag on each of his tractors. He also installed RFID readers next to his farm's roads so that he can see in which camps his various tractors are. The farmer requested an application that can collect the fuel consumption data of his tractors, as well as the location of the tractors and display this on an easy to use user interface. The data must also be stored for future analysis. 

JADE (Java Agent DEvelopment Framework) was used to develop an agent-based solution. The agents and how they communicated is shown in the figure below. A more in depth explanation of the solution can be found in doc/Reconfigurable control report_Agents.pdf.
<img src="/doc/RM1.JPG" height="600">
