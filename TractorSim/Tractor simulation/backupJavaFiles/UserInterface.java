import java.awt.Component;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JPanel;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class UserInterface extends Agent{
	private TractorsGUI myGui;
	
protected void setup() {
	DFAgentDescription dfd = new DFAgentDescription();
	dfd.setName(getAID());
	ServiceDescription sd = new ServiceDescription();
	sd.setType("DisplayTractorData");
	sd.setName("Tractor-DigitalTwins");
	dfd.addServices(sd);
	try {
		DFService.register(this, dfd);
	}
	catch (FIPAException fe) {
		fe.printStackTrace();
	}
	myGui = new TractorsGUI(this);
	myGui.showGui();
	addBehaviour(new TractorServer());
	
	
}
private class TractorServer extends CyclicBehaviour {
	private int farmID;
	public void action() {
		MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
		ACLMessage msg = myAgent.receive(mt);
		if (msg != null) {
			System.out.println("msg not null");
			// Read tractor info
			String message = msg.getContent();
			System.out.println(message);
            JAXBContext jaxbContext = null;
			try {
				jaxbContext = JAXBContext.newInstance(Tractor.class);
			} catch (JAXBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            Unmarshaller jaxbUnmarshaller = null;
			try {
				jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			} catch (JAXBException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
            StringReader sr = new StringReader(message);
            TractorDataXML T = null;
			try {
				T = (TractorDataXML)jaxbUnmarshaller.unmarshal(sr);
			} catch (JAXBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            try {
            	  farmID = Integer.parseInt(T.Farm.substring(4))-1;
            	  if((farmID>=0)&&(farmID<4)) {
                 	 myGui.updateGui(farmID,T.Row-1,T.Column-1,T.TractorID,T.FuelConsumption);
                 }else {
                 	System.out.println("CORRUPT DATA");
                 }
            }catch(Exception e){
            	System.out.println("Not valid farm");
            }
           
          
           
		}
		else {
			block();
		}
	}
}


}
