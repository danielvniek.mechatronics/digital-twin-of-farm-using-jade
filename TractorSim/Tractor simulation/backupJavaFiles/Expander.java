
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.AgentContainer;
import jade.core.ContainerID;
import jade.core.behaviours.ActionExecutor;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.OutcomeManager;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.JADEAgentManagement.CreateAgent;
import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREInitiator;

public class Expander extends Agent{
	private int nrTractors = 0;
	private int nrFarms = 0;
	String containerName = AgentContainer.MAIN_CONTAINER_NAME;
	

	protected void setup() {
		String startupData = "";
		String[] splitData = new String[2];
		 try {
	            FileReader reader = new FileReader("StartupFile.txt");
	            int character; 	            
	            while ((character = reader.read()) != -1) {
	                startupData = startupData + ((char) character);
	            }
	            reader.close();	 
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		splitData = startupData.split(",");
		nrFarms = Integer.parseInt(splitData[0]);
		nrTractors = Integer.parseInt(splitData[1]);
		System.out.println("Farms:"+nrFarms +"Tractors:"+ nrTractors);
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Expand");
		sd.setName("Tractor-DigitalTwins");
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
		addBehaviour(new StartUp());
		addBehaviour(new ExpansionServer());
		
	}
	private class StartUp extends OneShotBehaviour{
		String agentName;
		String agentClass;
		String agentArgument;
		public void action() {
			agentName = "FileSaver";
			agentClass = "FileSaver";
			agentArgument = "";
			AgentCreator(agentName, agentClass, agentArgument, false);
			agentName = "UserInterface";
			agentClass = "UserInterface";
			agentArgument = "";
			AgentCreator(agentName, agentClass, agentArgument, false);
			agentClass = "Farm";
			for(int f = 1;f<=nrFarms;f++) {
				agentArgument = String.valueOf(f);
				agentName = "Farm"+agentArgument;
				AgentCreator(agentName, agentClass, agentArgument, false);
			}
			
			agentClass = "Tractor";
			for(int t = 1;t<=nrTractors;t++) {
				agentArgument = String.valueOf(t);
				agentName = "Tractor"+agentArgument;
				AgentCreator(agentName, agentClass, agentArgument, false);
			}
			
			agentClass = "FuelSensor";
			for(int t = 1;t<=nrTractors;t++) {
				agentArgument = String.valueOf(t);
				agentName = "FuelSensor"+agentArgument;
				AgentCreator(agentName, agentClass, agentArgument, false);
			}
			
		}
	}
	private void AgentCreator(String agentName, String agentClass, String agentArgument, boolean dynamicAddition){
		CreateAgent ca = new CreateAgent();
		ca.setAgentName(agentName);
		ca.setClassName(agentClass);
		if(!agentArgument.equals("")) {
			ca.addArguments(agentArgument);
		}		
		ca.setContainer(new ContainerID(containerName, null));
		ActionExecutor<CreateAgent, Void> ae = new ActionExecutor<CreateAgent, Void>(ca, JADEManagementOntology.getInstance(), getAMS()) {
			@Override
			public int onEnd() {
				int ret = super.onEnd();
				if (getExitCode() == OutcomeManager.OK) {
					// Creation successful
					System.out.println("Agent successfully created");
					if(dynamicAddition == true) {
						if(agentClass.equals("Farm")) {
							nrFarms++;
						}
						if(agentClass.equals("Tractor")) {
							nrTractors++;
						}
						//update startup file
						FileWriter writer = null;
						try {
							writer = new FileWriter("StartupFile.txt",false);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							writer.write(String.valueOf(nrFarms)+","+String.valueOf(nrTractors));
							System.out.println("UPDATING STARTUP UPDATING STARTUP");
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							writer.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}
				else {
					// Something went wrong
					System.out.println("Agent creation error. "+getErrorMsg());
					//inform user interface
				}
				return ret;
			}
		};
		addBehaviour(ae);	
	}
	
	private class ExpansionServer extends CyclicBehaviour {
		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
			ACLMessage req = myAgent.receive(mt);
			if (req != null) {
				System.out.println("EXPANDER");
				// Read tractor info
				String message = req.getContent();
				ACLMessage reply = req.createReply();
	            if(message.equals("newTractor")) {
	            	String tractorID = "Tractor"+String.valueOf(nrTractors+1);
	            	//use AMS to spawn new tractor and fuel sensors
	            	String agentName = tractorID;
	            	String agentClass = "Tractor";
	            	String agentArgument = tractorID.substring(7);
	            	AgentCreator(agentName, agentClass, agentArgument, true);
	            	//let FileSaver know of new tractor
	            	DFAgentDescription template = new DFAgentDescription();
					ServiceDescription sd = new ServiceDescription();
					sd.setType("SaveTractorData");
					System.out.println(sd.getType());
					template.addServices(sd);
					try {
						DFAgentDescription[] result = DFService.search(myAgent, template); 
						System.out.println("EXPANDER: Found the following saving agents for "+getAID().getLocalName()+":");
						if(result.length==1) {
							ACLMessage newT = new ACLMessage(ACLMessage.REQUEST);
							newT.addReceiver(result[0].getName());
							newT.setContent("new:"+tractorID);
							myAgent.send(newT);
						}
						
					}catch (FIPAException fe) {
						fe.printStackTrace();
					}
	            }
	            if(message.contentEquals("newFarm")) {
	            	String farmID = "Farm"+String.valueOf(nrFarms+1);
	            	//use AMS to spawn new tractor and fuel sensors
	            	String agentName = farmID;
	            	String agentClass = "Farm";
	            	String agentArgument = farmID.substring(4);
	            	AgentCreator(agentName, agentClass, agentArgument, true);
	            }
	          
			}
			else {
				block();
			}
		}
	}
	//can only come from filesaver
	private class FailureServer extends CyclicBehaviour {
		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.FAILURE);
			ACLMessage failure = myAgent.receive(mt);
			if (failure != null) {
				//inform UserInterface that new tractor could not be added because there was a problem with updating the TractorData excel file
			}
			else {
				block();
			}
		}
	}
}
